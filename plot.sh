#!/bin/bash

set -euo pipefail

RECORD_FILE="cpu-heat.txt"
OUTFILE="cpu-temperature.png"
gnuplot -c a64plot.gpm "${RECORD_FILE}" "${OUTFILE}"
xdg-open "${OUTFILE}"
