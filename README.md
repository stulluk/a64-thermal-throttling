# a64-thermal-throttling

Simple bash & gnuplot for drawing thermal throttling on Allwinner A64 Android devices

# Prequisities

- gnuplot
- adb connection with root priviledge to A64 device

# Purpose

I wanted to see CPU Thermal throttling on my BPI-M64 under Android 7.1.1 OS

# Using / Running the test

```
./watchtemp.sh 1800
```

Or you can enter any number of seconds as argument to script.

After data is collected, plot it:

```
./plot.sh
```

# Sample

Below is tested on Bananapi-M64 with adb over IP with root

![](sample/cpu-temperature.png)



