#!/bin/bash

set -euo pipefail

function clean_exit {

	adb shell "killall -9 cpueater 2>/dev/null" || true
}
# Call the egress function
trap clean_exit EXIT

if [ $# -ne "1" ]
then
	printf "Usage: %s DURATION ( in SECONDS)\n" "$(basename $0)"
	exit 1
fi

TEST_DURATION=${1}

#Stop cpueaters if already working..
adb shell "killall -9 cpueater 2>/dev/null" || true

#Start cpueaters
adb shell "cpueater && cpueater && cpueater && cpueater"

RECORD_FILE="cpu-heat.txt"
time=0
TEST_OVER=0
printf "time,temp,cpu0_freq,cpu1_freq,cpu2_freq,cpu3_freq\n" | tee "${RECORD_FILE}"
while true :
do
	cpu_temp=$(adb shell cat /sys/devices/virtual/thermal/thermal_zone0/temp)
	cpu0_freq=$(adb shell "if [ -e /sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq ]; then cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq; else echo 0; fi")
	cpu1_freq=$(adb shell "if [ -e /sys/devices/system/cpu/cpu1/cpufreq/scaling_cur_freq ]; then cat /sys/devices/system/cpu/cpu1/cpufreq/scaling_cur_freq; else echo 0; fi")
	cpu2_freq=$(adb shell "if [ -e /sys/devices/system/cpu/cpu2/cpufreq/scaling_cur_freq ]; then cat /sys/devices/system/cpu/cpu2/cpufreq/scaling_cur_freq; else echo 0; fi")
	cpu3_freq=$(adb shell "if [ -e /sys/devices/system/cpu/cpu3/cpufreq/scaling_cur_freq ]; then cat /sys/devices/system/cpu/cpu3/cpufreq/scaling_cur_freq; else echo 0; fi")
	printf "%d,%d,%d,%d,%d,%d\n" "${time}" "${cpu_temp}" "${cpu0_freq}" "${cpu1_freq}" "${cpu2_freq}" "${cpu3_freq}" | tee -a "${RECORD_FILE}"
	sleep 5
	(( time += 5 ))

	if [ "${time}" -eq "${TEST_DURATION}" ] && [ "${TEST_OVER}" -eq "0" ]
	then
		printf "Test duration is over!\n"
		TEST_OVER=1
		printf "Cooling down...\n"
		#Stop cpueaters
		adb shell "killall -9 cpueater 2>/dev/null" || true
	fi

	#printf "TEST_OVER is %d \n" "${TEST_OVER}"

	if [ "${TEST_OVER}" -ge "1" ]
	then
		printf "Cooling...\n"
		(( TEST_OVER +=1 ))
	fi

	if [ "${TEST_OVER}" -ge "6" ]
	then
		printf "Cooling is over...\n"
		printf "Stopping test...\n"
		break
	fi
done


